import re
import json
import base64
import quopri
import logging
import requests
import urlparse

from models import Track

logger = logging.getLogger(__name__)

from social.apps.django_app.default.models import UserSocialAuth

def scrape_gmail(google_social_auth_id, start, end):
    '''
    main function, fetches emails from gmail with
    different queries and processes results
    '''
    logger.info('Processing emails from %s to %s' % (start, end))
    google = UserSocialAuth.objects.get(pk=google_social_auth_id)

    all_emails = set()
    join_all_ids(all_emails, fetch_emails(google, start, end, 'youtube.com'))
    join_all_ids(all_emails, fetch_emails(google, start, end, 'youtu.be'))
    join_all_ids(all_emails, fetch_emails(google, start, end, 'soundcloud.com'))
    logger.info('Found %d distinct emails' % len(all_emails))
    fetch_youtube_video_ids(all_emails, google)
    logger.info('Scraping finished.')

def fetch_emails(google, start, end, query):
    '''
    does a query in gmail and returns message ids
    '''
    q = '%s after:%s before:%s' % (query, start, end)
    logger.debug('making request to gmail ' + q)
    email = google.uid
    access_token = google.extra_data['access_token']
    response = requests.get(
            'https://www.googleapis.com/gmail/v1/users/%s/messages' % email,
            params={'access_token': access_token,'q': q}
        )
    if response.status_code == 200:
        data = json.loads(response.text)
        if 'resultSizeEstimate' in data and data['resultSizeEstimate'] == 0:
            return []
        elif 'messages' in data:
            return data['messages']
        else:
            message = "no messages in %s" % response.text
            logger.error(message)
            return []
    else:
        message = 'response %d %s' % (response.status_code, response.text)
        logger.error(message)
        return []

def fetch_youtube_video_ids(messages_ids, google):
    '''
    fetches email contents, looks for urls and processes urls
    '''
    access_token = google.extra_data['access_token']
    email = google.uid
    user = google.user

    logger.info('fetching email contents for ' + email)

    #save_urls = set()
    seen_urls = set()

    for message_id in messages_ids:
        logger.debug('fetching content for message %s' % message_id)
        response = requests.get(
            'https://www.googleapis.com/gmail/v1/users/%s/messages/%s' % (email, message_id),
            params={'access_token': access_token,'format': 'raw'}
        )
        message = json.loads(response.text)
        message = base64.urlsafe_b64decode(message['raw'].encode('latin-1'))
        message = quopri.decodestring(message)
        regex = '(http|ftp|https)(://)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?'
        urls = re.findall(regex, message)
        urls = map(lambda x : ''.join(x), urls)
        email_urls = set(urls)
        urls = email_urls - seen_urls
        logger.info('found %d distinct urls - %d new - in email %s' % (len(email_urls), len(urls), message_id))

        for url in urls:
            if url in seen_urls: continue
            seen_urls.add(url)

            reduced_url = reduce_url(url)
            final_url = url
            # This line unfurls the URL with a HEAD request that follows redirects
            logger.debug('original url %s' % reduced_url)
            try:
                response = requests.head(url, allow_redirects=True)
            except Exception as e:
                logger.warn('%s - %s' % (e, url))
                continue

            if response.status_code == 200:
                final_url = response.url # The response contains the unfurled URL
                if url != final_url:
                    logger.debug('unfurled url ' + reduce_url(final_url))
            else:
                logger.info('status code %d for %s' %(response.status_code, url))

            if is_youtube(final_url) or is_soundcloud(final_url):
                logger.info('keeping %s from %s' % (reduced_url,message_id))
                #don't save, process immediately
                #save_urls.add(final_url)
                process_video_url(url, user)
            else:
                logger.debug('discarding ' + reduced_url)

    #logger.info('found %d interesting urls' % len(save_urls))
    #for url in save_urls:
    #    process_video_url(url, user)

def process_video_url(url, user):
    '''
    process one url
    '''
    reduced_url = reduce_url(url)
    if is_youtube(url):
        fetch_video_info(url, user, 'http://www.youtube.com/oembed', 'youtube')
    elif is_soundcloud(url):
        fetch_video_info(url, user, 'http://soundcloud.com/oembed', 'soundcloud')
    else:
        logger.debug('ignoring %s' % reduced_url)


def fetch_video_info(url, user, embed_service_url, track_type):
    '''
    fetches the video info from youtube/soundcloud
    '''
    reduced_url = reduce_url(url)
    if Track.objects.filter(link=url,user_id=user.id).exists():
        logger.info('track already exists for user %s - %s' % (user, reduced_url))
        return

    logger.debug('getting info for %s video %s' % (track_type, reduced_url))
    track = True
    try:
        response = requests.get(embed_service_url, params={'url': url,'format': 'json'})
    except Exception as e:
        logger.error(e)
        return
    if response.status_code != 200:
        logger.warn('status code %d for %s' % (response.status_code, reduced_url))
        return

    data = json.loads(response.text)
    title =  data['title'].encode('ascii','ignore') if 'title' in data else ''
    thumbnail = data['thumbnail_url'] if 'thumbnail_url' in data else ''
    author = data['author_name'] if 'author_url' in data else ''
    author_url = data['author_url'] if 'author_url' in data else ''
    html = data['html'] if 'html' in data else ''
    save_track_info(user, url, html, title, author, author_url, thumbnail, track_type)

def save_track_info(user, url, embed, title, author, author_url, thumbnail, track_type):
    '''
    saves the track info
    only if the video is not in the DB for that user
    '''
    reduced_url = reduce_url(url)
    if Track.objects.filter(link=url,user_id=user.id).exists():
        logger.info('track already exists for user %s - %s' % (user, reduced_url))
    else:
        logger.info('adding track for user %s - %s' % (user, reduced_url))
        track = Track(\
        user_id=user.id, \
        link=url, \
        embed=embed,\
        title=title, \
        author=author, \
        author_link=author_url, \
        thumbnail=thumbnail, \
        track_type=track_type)
        track.save()

### HELPER FUNCS ###
def join_all_ids(all_emails, emails):
    all_emails.update(map(lambda x: x['id'], emails))

def reduce_url(url, length=64):
    if len(url) > length:
        url = url[:(length-3)] + '...'
    return url
#TODO improve these filters
def is_youtube(url):
    return 'youtube' in url or 'youtu.be' in url
def is_soundcloud(url):
    return 'soundcloud' in url
